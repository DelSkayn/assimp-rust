use super::assimp_sys::{aiAttachLogStream,AiLogStream};

use std::sync::{Once,ONCE_INIT};
use std::cell::UnsafeCell;
use std::ffi::CStr;
use std::ptr;

static mut LOGGER: *mut Box<FnMut(&str)> = 0 as *mut Box<FnMut(&str)>;
static LOGGER_INIT: Once = ONCE_INIT;

unsafe extern "system" fn assimp_log(msg: *const i8, _: *mut i8) {
    let string = CStr::from_ptr(msg);
    let string = string.to_str()
        .unwrap_or("Could not convert c string for printing.")
        .split('\n')
        .next()
        .unwrap();
    (*LOGGER)(string);
}

pub fn init_logger<F>(f: F)
    where F: FnMut(&str) + 'static
{
    unsafe{
        LOGGER_INIT.call_once(||{
            let b: Box<FnMut(&str)> = Box::new(f);
            LOGGER = Box::into_raw(Box::new(b));
        });
        let val = Box::into_raw(Box::new(AiLogStream {
            callback: Some(assimp_log),
            user: ptr::null_mut(),
        }));
        aiAttachLogStream(val);
    }
}


