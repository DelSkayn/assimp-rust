use super::AiMesh;
use super::Material;
use std::slice;
use std::rc::Rc;

pub struct Mesh{
    pub vertices: Vec<[f32; 3]>,
    pub normals: Option<Vec<[f32; 3]>>,
    pub tangents: Option<Vec<[f32; 3]>>,
    pub bitangents: Option<Vec<[f32; 3]>>,
    pub texture_coords: Option<Vec<Vec<[f32; 2]>>>,
    pub faces: Vec<Vec<u32>>,
    pub material: Rc<Material>,
    //name: Option<String>,
}

impl Mesh{
    #[doc(hidden)]
    pub unsafe fn from_sys_mesh(m: *const AiMesh,mats: &[Rc<Material>]) -> Self{
        assert!(!m.is_null());
        let mesh = &(*m);
        let num_verts = mesh.num_vertices as usize;
        let vertices: Vec<_> = if !mesh.has_positions(){
            panic!("No positions found in model, not supported!");
        }else{
            slice::from_raw_parts(mesh.vertices, num_verts)
                .iter()
                .map(|e| [e.x, e.y, e.z])
                .collect()
        };
        let faces: Vec<Vec<u32>> = if !mesh.has_faces(){
            panic!("Found mesh whitout faces, not supported!");
        }else{
            slice::from_raw_parts(mesh.faces, mesh.num_faces as usize)
                .iter()
                .map(|f|{
                    slice::from_raw_parts(f.indices,f.num_indices as usize)
                        .iter()
                        .cloned()
                        .collect()
                }).collect()
        };
        let normals: Option<Vec<_>> = if !mesh.has_normals(){
            None
        }else{
            Some(slice::from_raw_parts(mesh.normals, num_verts)
                    .iter()
                    .map(|e| [e.x, e.y, e.z])
                    .collect())
        };
        let tangents: Option<Vec<_>> = if !mesh.has_tangents_and_bitangents(){
            warn!("[assimp] No tangents found ");
            None
        }else{
            Some(slice::from_raw_parts(mesh.tangents, num_verts)
                    .iter()
                    .map(|e| [e.x, e.y, e.z])
                    .collect())
        };
        let bitangents: Option<Vec<_>> = if !mesh.has_tangents_and_bitangents(){
            None
        }else{
            Some(slice::from_raw_parts(mesh.bitangents, num_verts)
                    .iter()
                    .map(|e| [e.x, e.y, e.z])
                    .collect())
        };
        let uv_coords: Vec<Vec<_>> = (0..8).into_iter()
            .map(|i|{
                if mesh.texture_coords[i].is_null(){
                    None
                }else if mesh.num_uv_components[i] != 2{
                    warn!("[assimp] Found uv components which does not have 2 components, not supported!");
                    None
                }else{
                    Some(
                        slice::from_raw_parts(mesh.texture_coords[i],num_verts)
                        .iter()
                        .map(|e| [e.x,e.y])
                        .collect())
                }
            }).filter(|e| e.is_some())
            .map(|e| e.unwrap())
            .collect();
        Mesh{
            vertices: vertices,
            normals: normals,
            tangents: tangents,
            bitangents: bitangents,
            texture_coords: if uv_coords.len() > 0 { Some(uv_coords) } else { None },
            faces: faces,
            material: mats[mesh.material_index as usize].clone(),
        }
    }
}
