use super::assimp_sys::{self,AiScene,AiMesh,AiPostProcessSteps};
use super::PostProcess;

use std::path::Path;
use std::ffi::{CString,CStr};
use std::rc::Rc;
use std::slice;
use std::mem;
use std::ptr;

use super::{Error,AssimpError};

mod mesh;
mod material;

pub use self::mesh::Mesh;
pub use self::material::{Material,Textures,TextureParams};

pub struct Scene{
    pub meshes: Vec<Mesh>,
    pub materials: Vec<Rc<Material>>,
}


impl Scene{

    pub fn from_mem<I: IntoIterator<Item = PostProcess>>(data: &[u8],flags: I) -> Result<Scene,Error>{
        let mut bit_flags = AiPostProcessSteps::empty();
        for flag in flags{
            trace!("[assimp] scene flags: {:?}",flag);
            bit_flags = bit_flags | AiPostProcessSteps::from_bits_truncate(flag as u32);
        }
        unsafe{
            let sys_scene = assimp_sys::aiImportFileFromMemory
                ( mem::transmute(data.as_ptr())
                , data.len() as u32
                , bit_flags
                , ptr::null());
            if sys_scene.is_null(){
                let err_ptr = assimp_sys::aiGetErrorString();
                if err_ptr != ptr::null(){
                    let error = CStr::from_ptr(err_ptr).to_str().unwrap();
                    Err(AssimpError::Str(error).into())
                }else{
                    Err(AssimpError::NoneReported.into())
                }
            }else{
                let res = Ok(Self::from_sys_scene(sys_scene));
                assimp_sys::aiFreeScene(sys_scene);
                res
            }
        }
    }

    pub fn from_path<P: AsRef<Path>,I: IntoIterator<Item = PostProcess>>(path: P,flags: I) -> Result<Scene,Error>{
        let path = path.as_ref();
        let path_c = CString::new(path.to_str().unwrap().as_bytes()).unwrap();
        let mut bit_flags = AiPostProcessSteps::empty();
        for flag in flags{
            trace!("[assimp] scene flags: {:?}",flag);
            bit_flags = bit_flags | AiPostProcessSteps::from_bits_truncate(flag as u32);
        }
        unsafe{
            let sys_scene = assimp_sys::aiImportFile(path_c.as_ptr(),bit_flags);
            if sys_scene.is_null(){
                let err_ptr = assimp_sys::aiGetErrorString();
                if err_ptr != ptr::null(){
                    let error = CStr::from_ptr(err_ptr).to_str().unwrap();
                    Err(AssimpError::Str(error).into())
                }else{
                    Err(AssimpError::NoneReported.into())
                }
            }else{
                let res = Ok(Self::from_sys_scene(sys_scene));
                assimp_sys::aiFreeScene(sys_scene);
                res
            }
        }
    }

    unsafe fn from_sys_scene(s:*const AiScene) -> Scene{
        assert!(!s.is_null());
        let s = &(*s);
        let materials: Vec<_> = slice::from_raw_parts(s.materials,s.num_materials as usize)
            .iter()
            .cloned()
            .map(|mat|{
                Rc::new(Material::from_sys_material(mat))
            }).collect();
        let meshes = slice::from_raw_parts(s.meshes,s.num_meshes as usize)
            .iter()
            .cloned()
            .map(|mesh| {
                Mesh::from_sys_mesh(mesh as *const _,&materials)
            }).collect();
        Scene{
            meshes: meshes,
            materials: Vec::new(),
        }
    }
}
