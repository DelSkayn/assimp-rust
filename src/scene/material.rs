use super::assimp_sys::{AiMaterial,AiString,AiPropertyTypeInfo};
use std::path::{Path,PathBuf};
use std::slice;
use std::mem;
use std::str;


///Defines alpha-blend flags.
///
///If you're familiar with OpenGL or D3D, these flags aren't new to you. They define how the final color value of a pixel is computed, basing on the previous color at that pixel and the new color value from the material. The blend formula is:
///```
///      SourceColor * SourceBlend + DestColor * DestBlend
///```
///where <DestColor> is the previous color in the framebuffer at this position and <SourceColor> is the material colro before the transparency calculation.
#[derive(Clone,Debug,Copy)]
pub enum BlendFunc{
    Additive,
    Def,
}

impl BlendFunc{
    fn from_int(i: i32) -> Self{
        match i {
            0 => BlendFunc::Def,
            1 => BlendFunc::Additive,
            _ => panic!("Invalid blendfunc integer!"),
        }
    }
}

///Defines some mixed flags for a particular texture.
///
///Usually you'll instruct your cg artists how textures have to look like
///... and how they will be processed in your application.
///However, if you use Assimp for completely generic loading purposes you might
///also need to process these flags in order to display as many 'unknown' 3D models as possible correctly.
#[derive(Clone,Debug,Copy)]
pub enum TextureFlags{
    Invert,
    UseAlpha,
    IgnoreAlpha
}

///Defines how UV coordinates outside the [0...1] range are handled.
///
///Commonly refered to as 'wrapping mode'.
#[derive(Clone,Debug,Copy)]
pub enum TextureMapMode{
    ///A texture coordinate u|v is translated to u%1|v%1.
    Wrap,
    ///texture coordinates outside [0...1] are clamped to the nearest valid value.
    Clamp,
    ///If the texture coordinates for a pixel are outside [0...1] the texture is not applied to that pixel.
    Decal,
    ///A texture coordinate u|v becomes u%1|v%1 if (u-(u%1))%2 is zero and 1-(u%1)|1-(v%1) otherwise.
    Mirror,
}

impl TextureMapMode{
    fn try_from_u32(v: u32) -> Option<Self>{
        match v {
            0x0 => Some(TextureMapMode::Wrap),
            0x1 => Some(TextureMapMode::Clamp),
            0x2 => Some(TextureMapMode::Mirror),
            0x3 => Some(TextureMapMode::Decal),
            _ => None,
        }
    }
}

///Defines how the mapping coords for a texture are generated.
///
///Real-time applications typically require full UV coordinates,
///so the use of the aiProcess_GenUVCoords step is highly recommended.
///It generates proper UV channels for non-UV mapped objects,
///as long as an accurate description how the mapping should look like (e.g spherical) is given.
#[derive(Clone,Debug,Copy)]
pub enum TextureMapping{
    UV,
    Sphere,
    Cylinder,
    Box,
    Plane,
    Other,
}

///
#[derive(Clone,Debug,Copy)]
pub enum TextureOp{
    Multiply,
    Add,
    Subtract,
    Divide,
    SmoothAdd,
    SignedAdd,
}

///Defines all shading models supported by the library.
///The list of shading modes has been taken from Blender. See Blender documentation for more information.
///The API does not distinguish between "specular" and "diffuse" shaders (thus the specular term for diffuse shading
///models like Oren-Nayar remains undefined).
///Again, this value is just a hint. Assimp tries to select the shader whose most common implementation matches the
///original rendering results of the 3D modeller which wrote a particular model as closely as possible.
#[repr(u32)]
#[derive(Clone,Debug,Copy)]
pub enum ShadingModel{
    ///Flat shading.
    ///
    ///Shading is done on per-face base, diffuse only. Also known as 'faceted shading'.
    Flat = 0x1,
    Gouraud = 0x2,
    Phong = 0x3,
    Blinn = 0x4,
    ///Also known as 'comic' shader.
    Toon = 0x5,
    ///Extension to standard Lambertian shading, taking the roughness of the material into account
    OrenNayar = 0x6,
    ///Extension to standard Lambertian shading, taking the "darkness" of the material into account
    Minnaert = 0x7,
    ///Special shader for metallic surfaces.
    CookTorrance = 0x8,
    ///Constant light influence of 1.0.
    NoShading = 0x9,
    Fresnel = 0xa,
}

impl ShadingModel{
    fn try_from_u32(v: u32) -> Option<Self>{
        match v {
            0x1 => Some(ShadingModel::Flat),
            0x2 => Some(ShadingModel::Gouraud),
            0x3 => Some(ShadingModel::Phong),
            0x4 => Some(ShadingModel::Blinn),
            0x5 => Some(ShadingModel::Toon),
            0x6 => Some(ShadingModel::OrenNayar),
            0x7 => Some(ShadingModel::Minnaert),
            0x8 => Some(ShadingModel::CookTorrance),
            0x9 => Some(ShadingModel::NoShading),
            0xa => Some(ShadingModel::Fresnel),
            _ => None,
        }
    }
}

#[derive(Clone,Debug)]
pub struct TextureParams{
    pub blend_func: Option<BlendFunc>,
    pub texture_mapping: Option<TextureMapping>,
    /// How is dealt with uv coords going outside [0..1]
    pub texture_map_mode: Option<TextureMapMode>,
    pub texture_op: Option<TextureOp>,
    pub path: Option<PathBuf>,
}

impl TextureParams{
    fn none() -> Self{
        TextureParams{
            blend_func: None,
            texture_mapping: None,
            texture_map_mode: None,
            texture_op: None,
            path: None,
        }
    }
}

#[derive(Clone,Debug)]
pub struct Textures{
    pub diffuse: Option<TextureParams>,
    pub specular: Option<TextureParams>,
    pub ambient: Option<TextureParams>,
    pub emissive: Option<TextureParams>,
    pub height: Option<TextureParams>,
    pub normals: Option<TextureParams>,
    pub shininess: Option<TextureParams>,
    pub opacity: Option<TextureParams>,
    pub displacment: Option<TextureParams>,
    pub lightmap: Option<TextureParams>,
    pub reflection: Option<TextureParams>,
}

#[derive(Clone,Debug)]
pub struct Material{
    pub blend_func: Option<BlendFunc>,
    pub bump_scaling: Option<f32>,
    pub ambient: Option<[f32; 3]>,
    pub diffuse: Option<[f32; 3]>,
    pub emissive: Option<[f32; 3]>,
    pub reflective: Option<[f32; 3]>,
    pub specular: Option<[f32; 3]>,
    pub transparent: Option<[f32; 3]>,
    pub enable_wireframe: Option<bool>,
    pub global_background_image: Option<PathBuf>,
    pub name: Option<String>,
    pub opacity: Option<f32>,
    pub reflectivity: Option<f32>,
    pub refracti: Option<f32>,
    pub shading_model: Option<ShadingModel>,
    pub shininess: Option<f32>,
    pub shininess_strenght: Option<f32>,
    pub textures: Textures,
}

impl Material{
    #[doc(hidden)]
    pub unsafe fn from_sys_material(mat: *mut AiMaterial) -> Self{
        assert!(!mat.is_null());
        let mat: &AiMaterial = &(*mat);
        let props = slice::from_raw_parts(mat.properties,mat.num_properties as usize);
        let mut res = Self::none();
        for prop in props{
            let prop = &(**prop);
            match prop.key.as_ref(){
                "$mat.blend" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<i32>());
                    assert!(res.blend_func.is_none());
                    let ptr: *mut i32 = mem::transmute(prop.data);
                    res.blend_func = Some(BlendFunc::from_int(*ptr));
                },
                "$mat.bumpscaling" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<f32>());
                    assert!(res.bump_scaling.is_none());
                    let ptr: *mut f32 = mem::transmute(prop.data);
                    res.bump_scaling = Some(*ptr);
                },
                "$clr.ambient" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<[f32; 3]>());
                    assert!(res.ambient.is_none());
                    let ptr: *mut [f32; 3] = mem::transmute(prop.data);
                    res.ambient = Some(*ptr);
                },
                "$clr.diffuse" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<[f32; 3]>());
                    assert!(res.diffuse.is_none());
                    let ptr: *mut [f32; 3] = mem::transmute(prop.data);
                    res.diffuse = Some(*ptr);
                },
                "$clr.emissive" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<[f32; 3]>());
                    assert!(res.emissive.is_none());
                    let ptr: *mut [f32; 3] = mem::transmute(prop.data);
                    res.emissive = Some(*ptr);
                },
                "$clr.reflective" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<[f32; 3]>());
                    assert!(res.reflective.is_none());
                    let ptr: *mut [f32; 3] = mem::transmute(prop.data);
                    res.reflective = Some(*ptr);
                },
                "$clr.specular" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<[f32; 3]>());
                    assert!(res.specular.is_none());
                    let ptr: *mut [f32; 3] = mem::transmute(prop.data);
                    res.specular = Some(*ptr);
                },
                "$clr.transparent" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<[f32; 3]>());
                    assert!(res.transparent.is_none());
                    let ptr: *mut [f32; 3] = mem::transmute(prop.data);
                    res.transparent = Some(*ptr);
                },
                "$mat.wireframe" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    // dont know the proper size of this property might be a size of 4
                    assert_eq!(prop.data_length as usize,mem::size_of::<bool>());
                    assert!(res.enable_wireframe.is_none());
                    let ptr: *mut bool = mem::transmute(prop.data);
                    res.enable_wireframe = Some(*ptr);
                },
                "$mat.opacity" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<f32>());
                    assert!(res.opacity.is_none());
                    let ptr: *mut f32 = mem::transmute(prop.data);
                    res.opacity = Some(*ptr);
                },
                "$mat.reflectivity" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<f32>());
                    assert!(res.reflectivity.is_none());
                    let ptr: *mut f32 = mem::transmute(prop.data);
                    res.reflectivity = Some(*ptr);
                },
                "$mat.refracti" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<f32>());
                    assert!(res.refracti.is_none());
                    let ptr: *mut f32= mem::transmute(prop.data);
                    res.refracti = Some(*ptr);
                },
                "$mat.shininess" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<f32>());
                    assert!(res.shininess.is_none());
                    let ptr: *mut f32= mem::transmute(prop.data);
                    res.shininess= Some(*ptr);
                },
                "$mat.shinpercent" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.data_length as usize,mem::size_of::<f32>());
                    assert!(res.shininess_strenght.is_none());
                    let ptr: *mut f32 = mem::transmute(prop.data);
                    res.refracti = Some(*ptr);
                },
                "?bg.global" => {
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.index,0);
                    assert!(res.global_background_image.is_none());
                    warn!("[assimp] global_background_image property not implemented");
                },
                "$tex.file" =>{
                    //texture path
                    assert_eq!(prop.index,0);
                    let tex = match prop.semantic{
                        0x1 => &mut res.textures.diffuse,
                        0x2 => &mut res.textures.specular,
                        0x3 => &mut res.textures.ambient,
                        0x4 => &mut res.textures.emissive,
                        0x5 => &mut res.textures.height,
                        0x6 => &mut res.textures.normals,
                        0x7 => &mut res.textures.shininess,
                        0x8 => &mut res.textures.opacity,
                        0x9 => &mut res.textures.displacment,
                        0xa => &mut res.textures.lightmap,
                        0xb => &mut res.textures.reflection,
                        _ => panic!("Unrecgonized texture not supported!")
                    };
                    if tex.is_none(){
                        *tex = Some(TextureParams::none());
                    }
                    let tex = tex.as_mut().unwrap();
                    assert!(tex.path.is_none());
                    // for Some reason the pointer given does not point directly to the proper point but at
                    // an offset, Tried using AiString but that does not seem to work.
                    // It seems the data is at an offset of 4 bytes with a 4 less bytes then
                    // prop.data_length including the null byte at the end of the c string.
                    let data = slice::from_raw_parts(prop.data.offset(4) as *mut u8,(prop.data_length-5) as usize);
                    let s = str::from_utf8(data).unwrap();
                    debug!("Found texture path: {:?}",s);
                    let s = s.trim_left();
                    tex.path = Some(Path::new(s).to_path_buf());
                },
                "?mat.name" => {
                    let data = slice::from_raw_parts(prop.data.offset(4) as *mut u8,(prop.data_length-5) as usize);
                    let s = str::from_utf8(data).unwrap();
                    res.name = Some(s.trim_left().to_string());
                }
                "$mat.shadingm" => {
                    assert_eq!(prop.index,0);
                    assert_eq!(prop.semantic,0);
                    assert_eq!(prop.property_type,AiPropertyTypeInfo::Integer);
                    assert!(res.shading_model.is_none());
                    let ptr: *const u32 = mem::transmute(prop.data);
                    res.shading_model = Some(ShadingModel::try_from_u32(*ptr).unwrap())
                }
                "$tex.mapmodeu" =>{
                    assert_eq!(prop.index,0);
                    let tex = match prop.semantic{
                        0x1 => &mut res.textures.diffuse,
                        0x2 => &mut res.textures.specular,
                        0x3 => &mut res.textures.ambient,
                        0x4 => &mut res.textures.emissive,
                        0x5 => &mut res.textures.height,
                        0x6 => &mut res.textures.normals,
                        0x7 => &mut res.textures.shininess,
                        0x8 => &mut res.textures.opacity,
                        0x9 => &mut res.textures.displacment,
                        0xa => &mut res.textures.lightmap,
                        0xb => &mut res.textures.reflection,
                        _ => panic!("Unrecgonized texture not supported!")
                    };
                    if tex.is_none(){
                        *tex = Some(TextureParams::none());
                    }
                    let tex = tex.as_mut().unwrap();
                    assert!(tex.texture_map_mode.is_none());
                    let ptr: *const u32 = mem::transmute(prop.data);
                    tex.texture_map_mode = Some(TextureMapMode::try_from_u32(*ptr).unwrap())
                },
                x => { warn!("[assimp] Unrecgonized property in material: \"{}\".",x) },
            }
        }
        res
    }

    //   "$tex.uvtrafo" =>{
    //       //uv transform
    //   },
    //   "$tex.uvwsrc" =>{
    //   //uv uv source
    //   },
    //   "$tex.mapping" =>{
    //   },
    //   /*
    //   "$tex.mapmodev" =>{
    //   },
    //   "$tex.blend" =>{
    //   },
    //   "$tex.flags" =>{
    //   },
    //   "$tex.mapaxis" =>{
    //   },
    //   "$tex.op" =>{
    //   },

    pub fn none() -> Self{
        Material{
            blend_func: None,
            bump_scaling: None,
            ambient: None,
            diffuse: None,
            emissive: None,
            reflective: None,
            specular: None,
            transparent: None,
            enable_wireframe: None,
            global_background_image: None,
            name: None,
            opacity: None,
            reflectivity: None,
            refracti: None,
            shading_model: None,
            shininess: None,
            shininess_strenght: None,
            textures: Textures{
                diffuse: None,
                specular: None,
                ambient: None,
                emissive: None,
                height: None,
                normals: None,
                shininess: None,
                opacity: None,
                displacment: None,
                lightmap: None,
                reflection: None,
            },
        }
    }
}
