#![allow(unused_imports)]
#![allow(dead_code)]

use std::option::IntoIter as OptIter;

extern crate assimp_sys;
#[macro_use]
extern crate log;

pub mod scene;
pub use scene::{Scene,Material,Mesh};
pub mod logger;

use std::{fmt,io,error};

#[derive(Debug)]
pub enum AssimpError{
    NoneReported,
    Str(&'static str),
}

#[derive(Debug)]
pub enum Error{
    Io(io::Error),
    Assimp(AssimpError)
}

impl From<io::Error> for Error{
    fn from(e: io::Error) -> Self{
        Error::Io(e)
    }
}

impl From<AssimpError> for Error{
    fn from(e: AssimpError) -> Self{
        Error::Assimp(e)
    }
}

impl fmt::Display for Error{
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            Error::Io(ref e) => write!(f, "Io error: {}",e),
            Error::Assimp(ref e) => write!(f, "Error in assimp library: {}",e),
        }
    }
}

impl error::Error for Error{
    fn description(&self) -> &str{
        match *self{
            Error::Io(ref e) => e.description(),
            Error::Assimp(ref e) => e.description(),
        }
    }

    fn cause(&self) -> Option<&error::Error>{
        match *self{
            Error::Io(ref e) => Some(e),
            Error::Assimp(ref e) => Some(e),
        }
    }
}

impl error::Error for AssimpError{
    fn description(&self) -> &str{
        match *self{
            AssimpError::NoneReported => "Although the import failed no error was importered",
            AssimpError::Str(ref e) => e,
        }
    }
}

impl fmt::Display for AssimpError{
    fn fmt(&self,f: &mut fmt::Formatter) -> fmt::Result{
        match *self{
            AssimpError::NoneReported => write!(f, "No error was reported."),
            AssimpError::Str(x) => write!(f,"{}",x),
        }
    }
}

use std::slice::Iter as SliceIter;


static CONVERT_TO_LEFT_HANDED: &'static [PostProcess] =
&[ PostProcess::MakeLeftHanded
, PostProcess::FlipUvs
, PostProcess::FlipWindingOrder];

static REALTIME_FAST: &'static [PostProcess] =
&[ PostProcess::CalcTangentSpace
, PostProcess::GenNormals
, PostProcess::JoinIdenticalVertices
, PostProcess::Triangulate
, PostProcess::GenUvCoords
, PostProcess::SortByPType];

static REALTIME_MAX_QUALITY: &'static [PostProcess] =
&[ PostProcess::CalcTangentSpace
, PostProcess::GenSmoothNormals
, PostProcess::JoinIdenticalVertices
, PostProcess::Triangulate
, PostProcess::ImproveCacheLocality
, PostProcess::LimitBoneWeights
, PostProcess::RemoveRedundantMaterials
, PostProcess::SplitLargeMeshes
, PostProcess::GenUvCoords
, PostProcess::SortByPType
, PostProcess::FindDegenerates
, PostProcess::FindInvalidData
, PostProcess::FindInstances
, PostProcess::ValidateDataStructure
, PostProcess::OptimizeMeshes
, PostProcess::Debone ];

static REALTIME_QUALITY: &'static [PostProcess] =
&[ PostProcess::CalcTangentSpace
, PostProcess::GenSmoothNormals
, PostProcess::JoinIdenticalVertices
, PostProcess::Triangulate
, PostProcess::ImproveCacheLocality
, PostProcess::LimitBoneWeights
, PostProcess::RemoveRedundantMaterials
, PostProcess::SplitLargeMeshes
, PostProcess::GenUvCoords
, PostProcess::SortByPType
, PostProcess::FindDegenerates
, PostProcess::FindInvalidData ];

#[repr(u32)]
#[derive(Debug,Clone,Copy)]
pub enum PostProcess{
    CalcTangentSpace = 0x1,
    JoinIdenticalVertices = 0x2,
    MakeLeftHanded = 0x4,
    Triangulate = 0x8,
    RemoveComponent = 0x10,
    GenNormals = 0x20,
    GenSmoothNormals = 0x40,
    SplitLargeMeshes = 0x80,
    PreTransfromVertices = 0x100,
    LimitBoneWeights = 0x200,
    ValidateDataStructure = 0x400,
    ImproveCacheLocality = 0x800,
    RemoveRedundantMaterials = 0x1000,
    FixInfacingNormals = 0x2000,
    SortByPType = 0x8000,
    FindDegenerates = 0x10000,
    FindInvalidData = 0x20000,
    GenUvCoords = 0x40000,
    TransformUVCoords = 0x80000,
    FindInstances = 0x100000,
    OptimizeMeshes = 0x200000,
    OptimizeGraph = 0x400000,
    FlipUvs = 0x800000,
    FlipWindingOrder = 0x1000000,
    SplitByBoneCount = 0x2000000,
    Debone = 0x4000000,
}

pub struct Iter(SliceIter<'static,PostProcess>);

impl Iterator for Iter{
    type Item = PostProcess;

    fn next(&mut self) -> Option<PostProcess>{
        self.0.next().cloned()
    }

    fn size_hint(&self) -> (usize, Option<usize>){
        self.0.size_hint()
    }

    fn fold<Acc,F>(self, init: Acc, mut f: F) -> Acc
    where F: FnMut(Acc, Self::Item) -> Acc,
    {
        self.0.fold(init, move |acc, elt| f(acc,*elt))
    }
}

impl PostProcess{
    pub fn convert_to_left_hand() -> Iter{
        Iter(CONVERT_TO_LEFT_HANDED.iter())
    }

    pub fn fast() -> Iter{
        Iter(REALTIME_FAST.iter())
    }

    pub fn quality() -> Iter{
        Iter(REALTIME_QUALITY.iter())
    }

    pub fn max_quality() -> Iter{
        Iter(REALTIME_MAX_QUALITY.iter())
    }
}

impl IntoIterator for PostProcess{
    type Item = PostProcess;
    type IntoIter = OptIter<PostProcess>;

    fn into_iter(self) -> Self::IntoIter{
        Some(self).into_iter()
    }
}
